import unittest
from blog_app import app, db
from blog_app.models import User, Article
from config import TestingConfig


class HelpersFunctions():
    # Helpers functions
    name = "Test User"
    email = 'test@gmail.com'
    username = 'test'
    password = '123'
    confirm = '123'
    title = 'Test Post'
    post_body = 'Lorem ipsum dolor sit amet'

    def register(self, name, email, username, password, confirm):
        return self.client.post('/register', data=dict(
            name=name,
            email=email,
            username=username,
            password=password,
            confirm=confirm
        ), follow_redirects=True)

    def add_article(self, title, body):
        return self.client.post('/add-article', data=dict(
            title=title,
            body=body
        ), follow_redirects=True)

    def edit_article(self, id, title, body):
        return self.client.post('/edit-article/%d' % id, data=dict(
            title=title,
            body=body
        ), follow_redirects=True)

    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.client.get('/logout', follow_redirects=True)

    def create_test_user(self):
        test_user = User(self.name, self.email, self.username, self.password)
        db.session.add(test_user)
        db.session.commit()

    def create_test_article(self, title, body, username):
        test_article = Article(title, body, username)
        db.session.add(test_article)
        db.session.commit()


class BasicTests(unittest.TestCase, HelpersFunctions):
    # executed prior to each test

    @classmethod
    def setUpClass(cls):
        cls.app = app
        cls.app.config.from_object(TestingConfig)

    def setUp(self):
        self.client = self.app.test_client()
        db.session.remove()
        db.reflect()
        db.drop_all()
        db.create_all()

        # self.assertEqual(app.debug, False)

    # executed after each test
    def tearDown(self):
        db.session.remove()
        db.reflect()
        db.drop_all()

    # Tests

    def test_main_page(self):
        response = self.client.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_post_page(self):
        response = self.client.get('/posts', follow_redirects=True)
        self.assertIn(b'Posts', response.data)
        self.assertIn(b'Title', response.data)

    def test_unauthorized_dashboard_page(self):
        response = self.client.get('/dashboard', follow_redirects=True)
        self.assertIn(b'Unauthorized, Please login', response.data)

    def test_posts_on_dashboard(self):
        self.create_test_user()
        self.login(self.username, self.password)
        self.create_test_article(self.title, self.post_body, self.username)
        response = self.client.get('/dashboard', follow_redirects=True)
        self.assertIn(b'Dashboard', response.data)
        self.assertIn(self.title.encode(), response.data)

    def test_registration_page(self):
        response = self.client.get('/register', follow_redirects=True)
        self.assertIn(b'Registration', response.data)
        self.assertIn(b'Confirm Password', response.data)

    def test_login_page(self):
        response = self.client.get('/login', follow_redirects=True)
        self.assertIn(b'Login', response.data)
        self.assertIn(b'Username', response.data)
        self.assertIn(b'Password', response.data)
        self.assertIn(b'Submit', response.data)

    def test_wrong_login(self):
        response = self.login('test', 'test')
        self.assertIn(b'No such user', response.data)
        self.assertNotIn(b'Wrong password', response.data)

    def test_registration(self):
        self.assertIsNone(db.session.query(User).filter_by(username=self.username).one_or_none())

        response = self.register(self.name, self.email, self.username, self.password, self.confirm)
        self.assertIn(b'You are now registered and can log in', response.data)

        self.assertIn(
            db.session.query(User).filter_by(username=self.username).one_or_none(),
            User.query.all()
        )

        response = self.register(self.name, self.email, self.username, self.password, self.confirm)
        self.assertIn(b'Username or email already exists', response.data)

    def test_passwords_mismatch_during_registration(self):
        response = self.register(
            self.name,
            self.email,
            self.username,
            self.password,
            self.confirm + 'x'
        )
        self.assertIn(b'Passwords didnt match', response.data)

    def test_is_test_user_can_be_registered(self):
        self.create_test_user()
        self.assertIn(
            db.session.query(User).filter_by(username=self.username).one_or_none(),
            User.query.all()
        )

    def test_successful_login(self):
        self.create_test_user()
        response = self.login(self.username, self.password)
        self.assertIn(b'Login successful', response.data)

    def test_wrong_username_on_login(self):
        self.create_test_user()
        response = self.login(self.username + 'x', self.password)
        self.assertIn(b'No such user', response.data)
        self.assertNotIn(b'Wrong password', response.data)

    def test_wrong_password_on_login(self):
        self.create_test_user()
        response = self.login(self.username, self.password + 'x')
        self.assertIn(b'Wrong password', response.data)
        self.assertNotIn(b'No such user', response.data)

    def test_authorized_logout(self):
        self.create_test_user()
        self.login(self.username, self.password)
        response = self.logout()
        self.assertIn(b'You are now logged out', response.data)
        self.assertNotIn(b'Unauthorized, Please login', response.data)

    def test_unauthorized_logout(self):
        response = self.logout()
        self.assertIn(b'Unauthorized, Please login', response.data)
        self.assertNotIn(b'You are now logged out', response.data)

    def test_username_display_after_login(self):
        self.create_test_user()
        response = self.login(self.username, self.password)
        self.assertIn(self.username.encode(), response.data)

    def test_dashboard_for_autorized_user(self):
        self.create_test_user()
        self.login(self.username, self.password)
        response = self.client.get('/dashboard', follow_redirects=True)
        self.assertIn(b'Dashboard', response.data)
        self.assertIn(b'Add new article', response.data)

    def test_unauthorized_add_article_page(self):
        response = self.client.get('/add-article', follow_redirects=True)
        self.assertIn(b'Unauthorized, Please login', response.data)
        self.assertNotIn(b'Add article', response.data)

    def test_autorized_add_article_page(self):
        self.create_test_user()
        self.login(self.username, self.password)
        response = self.client.get('/add-article', follow_redirects=True)
        self.assertIn(b'Add article', response.data)
        self.assertNotIn(b'Unauthorized, Please login', response.data)

    def test_article_creation(self):
        self.create_test_user()
        self.login(self.username, self.password)
        response = self.add_article(self.title, self.post_body)
        self.assertIn(b'Article added', response.data)

    def test_unauthorized_ariticle_creation(self):
        response = self.add_article(self.title, self.post_body)
        self.assertIn(b'Unauthorized, Please login', response.data)
        self.assertNotIn(b'Article added', response.data)

    def test_posts_rendering(self):
        self.create_test_article(self.title, self.post_body, self.username)
        response = self.client.get('/posts', follow_redirects=True)
        self.assertIn(self.title.encode(), response.data)

    def test_singe_post_rendering(self):
        test_article = Article(self.title, self.post_body, self.username)
        db.session.add(test_article)
        db.session.commit()
        response = self.client.get('/posts/%d' % test_article.id, follow_redirects=True)
        self.assertIn(self.title.encode(), response.data)
        self.assertIn(self.post_body.encode(), response.data)

    def test_unauthorized_edit_page(self):
        test_article = Article(self.title, self.post_body, self.username)
        db.session.add(test_article)
        db.session.commit()
        response = self.client.get('/edit-article/%d' % test_article.id, follow_redirects=True)
        self.assertIn(b'Unauthorized, Please login', response.data)

    def test_edit_page(self):
        self.create_test_user()
        self.login(self.username, self.password)
        test_article = Article(self.title, self.post_body, self.username)
        db.session.add(test_article)
        db.session.commit()
        response = self.client.get('/edit-article/%d' % test_article.id, follow_redirects=True)
        self.assertIn(b'Edit article', response.data)
        self.assertIn(self.title.encode(), response.data)
        self.assertIn(self.post_body.encode(), response.data)

    def test_unathorized_editing(self):
        test_article = Article(self.title, self.post_body, self.username)
        db.session.add(test_article)
        db.session.commit()
        response = self.edit_article(test_article.id, self.title + 'x', self.post_body + 'x')
        self.assertIn(b'Unauthorized, Please login', response.data)

    def test_authorized_editing(self):
        self.create_test_user()
        self.login(self.username, self.password)
        self.create_test_article(self.title, self.post_body, self.username)
        self.edit_article(1, self.title + 'x', self.post_body + 'x')
        response = self.client.get('/posts/1', follow_redirects=True)
        self.assertIn((self.title + 'x').encode(), response.data)
        self.assertIn((self.post_body + 'x').encode(), response.data)

    def test_deleting_article(self):
        self.create_test_user()
        self.login(self.username, self.password)
        self.create_test_article(self.title, self.post_body, self.username)
        response = self.client.post('/delete-article/1', follow_redirects=True)
        self.assertIn(b'Article deleted', response.data)

    def test_unauthorized_deleting_article(self):
        self.create_test_article(self.title, self.post_body, self.username)
        response = self.client.post('/delete-article/1', follow_redirects=True)
        self.assertIn(b'Unauthorized, Please login', response.data)


if __name__ == "__main__":
    unittest.main()
