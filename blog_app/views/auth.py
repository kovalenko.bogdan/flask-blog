from flask import render_template, flash, redirect, url_for, request, Blueprint
from werkzeug import security
from blog_app.forms import RegisterForm, LoginForm
from sqlalchemy.exc import IntegrityError
from blog_app import db, app
from blog_app.models import User
from flask.views import MethodView
from blog_app import login_manager
from flask_login import login_user, login_required, logout_user

auth = Blueprint('auth', __name__)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.errorhandler(401)
def custom_401(error):
    flash('Unauthorized, Please login', 'danger')
    return redirect(url_for('auth.login'))


class UserRegistration(MethodView):
    """Validates users registration data ands new user to database"""
    def get(self):
        '''Render registration page'''
        form = RegisterForm(request.form)
        return render_template('register.html', form=form)

    def post(self):
        """Gets data from registration form and creates new users"""
        form = RegisterForm(request.form)
        if form.validate():
            try:
                # Create new user instance and add it to database
                new_user = User(form.name.data, form.email.data,
                                form.username.data, form.password.data)
                db.session.add(new_user)
                db.session.commit()
            except IntegrityError:
                # Handle db exception if 'username' or 'email' already exists in db
                db.session.rollback()
                flash("Username or email already exists", 'danger')
                return render_template('register.html', form=form)

            # Redirect to home when registration is done
            flash('You are now registered and can log in', 'success')
            return redirect(url_for('views.index'))
        else:
            flash('Passwords didnt match', 'danger')
            return redirect(url_for('auth.registration'))


class UserLogin(MethodView):
    """Handle user login and sessioning"""
    def get(self):
        """Render login page"""
        form = LoginForm(request.form)
        return render_template('login.html', form=form)

    def post(self):
        """Authorizes user and set sessions"""
        form = LoginForm(request.form)
        if form.validate():
            username = form.username.data
            password_candidate = form.password.data

            # Trying to find user with given username
            result = db.session.query(User).filter_by(username=username).one_or_none()
            if not result:
                flash('No such user', 'danger')
                return render_template('login.html', form=form)

            if security.check_password_hash(result.password, password_candidate):
                # Add sessions variable on login success
                login_user(result)
                flash('Login successful', 'success')
                return redirect(url_for('views.dashboard'))
            else:
                flash('Wrong password', 'danger')
                return render_template('login.html', form=form)
        else:
            flash('Wrong credentials', 'danger')
            return render_template('login.html', form=form)


class UserLogout(MethodView):
    """Logout user and clear session"""
    decorators = [login_required]

    def get(self):
        """Clears session and redirects to login page"""
        logout_user()
        flash("You are now logged out", "success")
        return redirect(url_for('auth.login'))


auth.add_url_rule('/register', view_func=UserRegistration.as_view('registration'))
auth.add_url_rule('/login', view_func=UserLogin.as_view('login'))
auth.add_url_rule('/logout', view_func=UserLogout.as_view('logout'))
