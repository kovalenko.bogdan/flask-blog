from flask import render_template, flash, redirect, url_for, request, Blueprint
from sqlalchemy import desc
from blog_app import db, admin
from blog_app.models import Article, User
from flask.views import MethodView, View
from flask_login import login_required, current_user
from flask_admin.contrib.sqla import ModelView

blog = Blueprint('views', __name__)


class BlogModelView(ModelView):

    def is_accessible(self):
        return current_user.is_authenticated


admin.add_view(BlogModelView(Article, db.session))
admin.add_view(BlogModelView(User, db.session))


class ShowHome(View):
    """Render Home page"""
    def dispatch_request(self):
        return render_template('home.html')


class ShowPosts(View):
    """Display all published articles"""
    def dispatch_request(self):
        """Gets posts from database and pass it to template"""
        Posts = Article.query.all()
        return render_template('posts.html', posts=Posts)


class ShowPost(View):
    """Displays individual article"""
    def dispatch_request(self, id):
        """Gets post with given id from database and pass it to template"""
        current_post = db.session.query(Article).filter_by(id=id).one_or_none()
        if not current_post:
            flash("No such article %s" % id, "danger")
            return redirect(url_for("views.posts"))
        return render_template('article.html', article=current_post)


class ShowDashboard(View):
    """Show dashboard to authorized users"""
    decorators = [login_required]

    def dispatch_request(self):
        """Show all post created by user"""
        if current_user.username == 'admin':
            authors_posts = db.session.query(Article).order_by(desc(Article.creation_time)).all()
        else:
            authors_posts = db.session.query(Article).filter_by(author=current_user.username) \
                .order_by(desc(Article.creation_time)).all()
        return render_template('dashboard.html', posts=authors_posts)


class PostAdd(MethodView):
    """Create new post in database"""
    decorators = [login_required]

    def get(self):
        """Render add article page"""
        return render_template('add-article.html')

    def post(self):
        """Creates new post object and add it to database"""
        title = request.form['title']
        body = request.form['body']
        author = current_user.username
        new_article = Article(title, body, author)
        db.session.add(new_article)
        db.session.commit()

        flash("Article added", "success")
        return redirect(url_for('views.dashboard'))


class PostEdit(MethodView):
    """Edit existing post"""
    decorators = [login_required]

    def get(self, id):
        """Show content from the existing post in the redactor page"""
        selected_article = db.session.query(Article).filter_by(id=id).one_or_none()
        return render_template('edit-article.html', article=selected_article)

    def post(self, id):
        """Updates post with submitted data"""
        selected_article = db.session.query(Article).filter_by(id=id).one_or_none()
        # Check if article exists
        if not selected_article:
            flash("No such article with id: %s" % id, "danger")
            return redirect(url_for("views.dashboard"))

        # Check user that perform post updating
        if selected_article.author != current_user.username and current_user.username != 'admin':
            flash("You are not allowed to edit this article", "danger")
            return redirect(url_for("views.dashboard"))

        # Updating post object and commit it to database
        title = request.form['title']
        body = request.form['body']

        selected_article.title = title
        selected_article.body = body

        db.session.commit()

        flash("Article updated", "success")
        return redirect(url_for('views.dashboard'))


class PostDelete(MethodView):
    """Deleting post"""
    decorators = [login_required]

    def post(self, id):
        """Validates user request and deletes post from database"""
        selected_article = db.session.query(Article).filter_by(id=id).one_or_none()
        # Check if the article with given id exists in database
        if not selected_article:
            flash("No such article with id: %s" % id, "danger")
            return redirect(url_for("views.dashboard"))

        # Check is the action performed by post author
        if selected_article.author != current_user.username and current_user.username != 'admin':
            flash("You are not allowed to edit this article", "danger")
            return redirect(url_for("views.dashboard"))

        # Delete from database
        db.session.delete(selected_article)
        db.session.commit()

        flash("Article deleted", "danger")
        return redirect(url_for('views.dashboard'))


blog.add_url_rule('/', view_func=ShowHome.as_view('index'))
blog.add_url_rule('/posts/', view_func=ShowPosts.as_view('posts'))
blog.add_url_rule('/posts/<string:id>', view_func=ShowPost.as_view('article'))
blog.add_url_rule('/add-article', view_func=PostAdd.as_view('add_article'))
blog.add_url_rule('/dashboard', view_func=ShowDashboard.as_view('dashboard'))
blog.add_url_rule('/edit-article/<string:id>', view_func=PostEdit.as_view('edit_article'))
blog.add_url_rule('/delete-article/<string:id>', view_func=PostDelete.as_view('delete_article'))
