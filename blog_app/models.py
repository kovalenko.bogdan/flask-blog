from blog_app import db
from datetime import datetime
from werkzeug import security
from flask_login import UserMixin


class User(UserMixin, db.Model):
    '''Define users model'''
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100), unique=True)
    username = db.Column(db.String(30), unique=True)
    password = db.Column(db.String(100))
    register_date = db.Column(db.DateTime())

    def __init__(self, name, email, username, password):
        self.name = name
        self.email = email
        self.username = username
        self.password = security.generate_password_hash(str(password))
        self.register_date = datetime.now()

    def __repr__(self):
        return '<user {}>'.format(self.name)


class Article(db.Model):
    '''Define article model'''
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    body = db.Column(db.Text())
    author = db.Column(db.String(100))
    creation_time = db.Column(db.DateTime())

    def __init__(self, title, body, author):
        self.title = title
        self.body = body
        self.author = author
        self.creation_time = datetime.now()

    def __repr__(self):
        return '<id {}>'.format(self.id)
