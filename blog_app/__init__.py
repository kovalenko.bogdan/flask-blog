import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_login import LoginManager
from flask_admin import Admin
from flask_migrate import Migrate, MigrateCommand
import configparser


config = configparser.ConfigParser()
config.read("config.ini")

app = Flask(__name__)
app.config.from_object(os.environ.get('APP_SETTINGS') or config["APP"].get("APP_SETTINGS", ""))
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
admin = Admin(app)
from .views.blog import blog
app.register_blueprint(blog)
from .views.auth import auth
app.register_blueprint(auth)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)
