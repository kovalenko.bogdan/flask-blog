"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count


import os
import configparser

# Read config.ini file
config = configparser.ConfigParser()
config.read("config.ini")

TMP = os.path.expanduser("./tmp/")
if not os.path.exists(TMP):
    os.mkdir('tmp')


port = config["GUNICORN"].getint("PORT", 8000)
bind = config["GUNICORN"].get("BIND", "127.0.0.1:%s" % port)
workers = config["GUNICORN"].getint("WORKERS", cpu_count())
worker_class = config["GUNICORN"].get("WORKER_CLASS", "gevent")
max_requests = config["GUNICORN"].getint("MAX_REQUESTS", 1000)
daemon = config["GUNICORN"].getboolean("DAEMON", True)
pidfile = config["GUNICORN"].get("PIDFILE", TMP + "flask-blog.pid")
accesslog = config["GUNICORN"].get("ACCESSLOG", TMP + "flask-blog.access.log")
errorlog = config["GUNICORN"].get("ERRORLOG", TMP + "flask-blog.error.log")
graceful_timeout = config["GUNICORN"].getint("GRACEFUL_TIMEOUT", 60)
timeout = config["GUNICORN"].getint("TIMEOUT", 300)
keepalive = config["GUNICORN"].getint("KEEPALIVE", 0)

# Config destructor
del config
