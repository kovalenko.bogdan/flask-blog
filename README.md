### Stage server

[flask-blog-stage](https://flask-blog-stage.herokuapp.com/)

### Production server

[flask-blog-prod](https://flask-blog-prod.herokuapp.com/)

### Migrations

#### Local
- generate migrations on your local machine `python manage.py db migrate`
- apply migration `python manage.py db upgrade`

#### Staging
- commit this changes
- push to heroku stage `git push stage master`
- apply migrations on remote server 
```heroku run python manage.py db upgrade --app flask-blog-stage```

#### Dev
- push to heroku prod `git push prod master`
- ```heroku run python manage.py db upgrade --app flask-blog-prod```