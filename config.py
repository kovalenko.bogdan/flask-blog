import os.path
import configparser
import tempfile
import shutil


basedir = os.path.abspath(os.path.dirname(__file__))

config = configparser.ConfigParser()
if not os.path.exists("config.ini"):
    shutil.copyfile("config.ini.example", "config.ini")
config.read("config.ini")


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = (os.environ.get('DATABASE_URL') or
                              config["APP"].get("DATABASE_URL", ""))
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(object):
    DEBUG = False
    SECRET_KEY = 'this-really-needs-to-be-changed'
    TESTING = True
    db_fd, db_path = tempfile.mkstemp()
    SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % db_path
    WTF_CSRF_ENABLED = True
    CSRF_ENABLED = True
